<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use JWTAuth;
use DB;
// use App\User;

class StudentController extends Controller
{

    protected $user;

// public function __construct()
// {
//     // $this->user = JWTAuth::parseToken()->authenticate();
//     $this->middleware('auth:api',['except'=>['index']]);
//     // $this->middleware('auth:api');
// }


public function index()
{
    //return Student::get(['id','name', 'age','mobile','address','stream_id','subject_id'])->toArray();
    return Student::all();

    // return DB::table('students')
    //         ->LeftJoin('streams', 'students.stream_id', '=', 'streams.stream_id')
    //         ->LeftJoin('subjects', 'students.subject_id', '=', 'subjects.sub_id')
    //         ->get();

    // return Students::join('students', 'stream.id', '=', 'students.post_id')
    // ->join('likes', 'posts.id', '=', 'likes.post_id')
    // ->join('images', 'posts.id', '=', 'images.post_id')
    // ->get();}
}

public function show($id)
{
    $student = Student::find($id);

    if (!$student) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, student with id ' . $id . ' cannot be found'
        ], 400);
    }

    return $student;
}

public function store(Request $request)
{
    $this->validate($request, [
        'name' => 'required',
        'age' => 'required|integer',
        'mobile' => 'required|integer',
        'address' => 'required',
        'stream_id' => 'required',
        'subject_id' => 'required'
    ]);

    $student = new Student();
    $student->name = $request->name;
    $student->age = $request->age;
    $student->mobile = $request->mobile;
    $student->address = $request->address;
    $student->stream_id = $request->stream_id;
    $student->subject_id = $request->subject_id;




    if (Student::save($student))
        return response()->json([
            'success' => true,
            'student' => $student
        ]);
    else
        return response()->json([
            'success' => false,
            'message' => 'Sorry, student could not be added'
        ], 500);
}


public function update(Request $request, $id)
{
    $student = Student::find($id);

    if (!$student) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, student with id ' . $id . ' cannot be found'
        ], 400);
    }

    $updated = $student->fill($request->all())
        ->save();

    if ($updated) {
        return response()->json([
            'success' => true
        ]);
    } else {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, student could not be updated'
        ], 500);
    }
}



public function destroy($id)
{
    $student = Student::find($id);

    if (!$student) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, student with id ' . $id . ' cannot be found'
        ], 400);
    }

    if ($student->delete()) {
        return response()->json([
            'success' => true
        ]);
    } else {
        return response()->json([
            'success' => false,
            'message' => 'student could not be deleted'
        ], 500);
    }
}

// public function students()
//     {
//     return (Student::class);
//     }


}
