<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'age', 'address','mobile','stream_id','subject_id'
    ];
}
