<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

Route::group(['middleware' => 'auth.jwt'], function () {

    Route::get('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');
    Route::get('students', 'StudentController@index');
    Route::get('students/{id}', 'StudentController@show');
    Route::post('students', 'StudentController@store');
    Route::put('students/{id}', 'StudentController@update');
    Route::delete('students/{id}', 'StudentController@destroy');
});
